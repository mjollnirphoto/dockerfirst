# Proyecto de Docker for <span style="color:blue">Rubiko</span>

Este repo contiene los archivos para dockerizar un servidor web sencillo con NodeJS y Express.

---

## Instrucciones :

![Pequeña Imagen de instrucciones](https://media.tenor.com/WjVCSz8fZSAAAAAM/estudar-estudo.gif)

### Ejecutar con Docker ⚙

1. Asegúrate de tener Docker instalado en tu sistema ( en mi caso tuve que bajar una versión antigua para el sistema Mac OS X El Capitan ).
2. Clona o baja este repositorio de git .
3. Vé al directorio .
4. Construye la imagen Docker :

```
   `docker build -t rubiko-basic-docker `
```

5. Ejecuta el contenedor :

```
   `docker run -p 5050:5000 -e GREETINGS="Hello Rubiko Tech!" rubiko-basic-docker`
```

Puedes acceder a la aplicación en tu navegador o mediante cURL: `http://localhost:5050/health`

### Ejecutar con Docker Compose

1. Asegúrate de tener Docker Compose instalado en tu sistema.
2. Navega al directorio del repositorio
3. Ejecuta Docker Compose:

```
   `docker-compose up`
```

La aplicación estará disponible en `http://localhost:5050/health`.

## Avanzado

Si deseas personalizar el saludo, puedes modificar la variable de entorno `GREETINGS` en el archivo `docker-compose.yml`.

---

![imagen the end](https://24.media.tumblr.com/cd425d0ad8cf11b1f1b960e0c017a84b/tumblr_mngvznvQc71s0o1b4o1_500.gif)
